import React, { useState } from 'react';
import { Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import './App.css';
import Face from './Face'
import Tile from './Tile'
import DifficultyButton from './DifficultyButton'

const beginnerGameSettings = {
  rows: 9,
  columns: 9,
  mines: 10,
  name: "😺 Beginner"
};

const intermediateGameSettings = {
  rows: 16,
  columns: 16,
  mines: 40,
  name: "🐆 Intermediate"
};

const expertGameSettings = {
  rows: 16,
  columns: 30,
  mines: 99,
  name: "🦁 Expert"
};

const difficultyLevels = [beginnerGameSettings, intermediateGameSettings, expertGameSettings];

const generateEmptyGrid = gameSettings => {
  const newGrid = [];
  for (let i = 0; i < gameSettings.rows; i++) {
    const newRow = [];
    for (let j = 0; j < gameSettings.columns; j++) {
      const newTile = {
        mined: false,
        status: 'hidden', // hidden / flagged / shown
        adjacentMines: 0
      };
      newRow.push(newTile);
    }
    newGrid.push(newRow);
  }
  return newGrid;
};

const countAdjacentXAt = (grid, gameSettings, x, y, predicate) => {
  let minesNear = 0;
  for (let i = (y === 0 ? 0 : -1); i <= ((y+1) === gameSettings.rows ? 0 : 1); i++)
    for (let j = (x === 0 ? 0 : -1); j <= ((x+1) === gameSettings.columns ? 0 : 1); j++)
      if (predicate(grid[y+i][x+j]) === true)
        minesNear++;
  return minesNear;
}

const countAdjacentMinesAt = (grid, gameSettings, x, y) =>
  countAdjacentXAt(grid, gameSettings, x, y, tile => tile.mined === true);

const countAdjacentFlagsAt = (grid, gameSettings, x, y) =>
  countAdjacentXAt(grid, gameSettings, x, y, tile => tile.status === 'flagged');

const generateGridWithMines = (gameSettings, x, y, newGrid) => {

  // generate mines

  let minesLeft = gameSettings.mines;

  while (minesLeft > 0)
  {
    const randomX = randomInteger(0, gameSettings.columns - 1);
    const randomY = randomInteger(0, gameSettings.rows - 1);

    const tile = newGrid[randomY][randomX];

    // tile isn't valid to place a mine if
    // (a) it already contains a mine, or
    // (b) it is the first tile that the player clicked

    if (tile.mined === true || (x === randomX && y === randomY))
      continue;
    else {
      tile.mined = true;
      minesLeft--;
    }
  }

  // calculate adjacent mines

  for (let i = 0; i < gameSettings.rows; i++) {
    for (let j = 0; j < gameSettings.columns; j++) {
      newGrid[i][j].adjacentMines = countAdjacentMinesAt(newGrid, gameSettings, j, i);
    }
  }

  // return

  return newGrid;
};

// ref: https://stackoverflow.com/a/2998874/983064
const zeroPad = (num, places) => String(num).padStart(places, '0');

// ref: https://stackoverflow.com/a/24152886/983064
const randomInteger = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

function App() {

  const [gameSettings, setGameSettings] = useState(beginnerGameSettings);
  const [grid, setGrid] = useState(generateEmptyGrid(gameSettings));
  const [debug] = useState(false);
  const [gameState, setGameState] = useState('notStarted'); // notStarted / started / lost / won
  const [flagsUsed, setFlagsUsed] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [clock, setClock] = useState(0);
  const [statusMessage, setStatusMessage] = useState('');
  const [statusSeverity, setStatusSeverity] = useState('');

  const clearAdjacentTilesAt = (x, y, newGrid) => {
    for (let i = (x === 0 ? 0 : -1); i <= ((x+1) === gameSettings.columns ? 0 : 1); i++)
      for (let j = (y === 0 ? 0 : -1); j <= ((y+1) === gameSettings.rows ? 0 : 1); j++)
        clearTileAt(x + i, y + j, newGrid);
  }

  const showAllMines = newGrid => {
    for (let i = 0; i < gameSettings.rows; i++) {
      for (let j = 0; j < gameSettings.columns; j++) {
        const tile = newGrid[i][j];
        if (tile.mined === true && tile.status !== 'flagged')
          tile.status = 'shown';
        else if (tile.mined === false && tile.status === 'flagged')
          tile.wrong = true;
      }
    }
  };

  const checkIfWon = newGrid => {
    let hiddenTiles = 0;
    for (let i = 0; i < gameSettings.rows; i++) {
      for (let j = 0; j < gameSettings.columns; j++) {
        const tile = newGrid[i][j];
        if (tile.status === 'hidden' || tile.status === 'flagged')
          hiddenTiles++;
      }
    }

    return hiddenTiles === gameSettings.mines;
  };

  const clearTileAt = (x, y, newGrid) => {
    const tile = newGrid[y][x];

    if (tile.status === 'hidden') {
      tile.status = 'shown';

      if (tile.mined) {
        tile.exploded = true;
        showAllMines(newGrid);
        setGameState('lost');
        clearInterval(clock);
        setStatusSeverity('error');
        setStatusMessage('Too bad! You lost! 😢');
      }
      else {
        if (checkIfWon(newGrid) === true) {
          setGameState('won');
          clearInterval(clock);
          setStatusSeverity('success');
          setStatusMessage('Congratulations! You won! 🎉');
        }
        else if (tile.adjacentMines === 0) { // clear tiles around
          clearAdjacentTilesAt(x, y, newGrid);
        }
      }
    }
  }

  const longPressTile = (x, y) => {
    const tile = grid[y][x];

    if (tile.status === 'hidden' || tile.status === 'flagged')
      rightClickTile(x, y); // longpress on hidden => flag
    else if (tile.status === 'shown')
      comboClickTile(x, y); // longpress on number => clear
  };

  const comboClickTile = (x, y) => {
    // copy the grid
    let newGrid = grid.map(row => row.map(tile => ({...tile})));

    const adjacentFlags = countAdjacentFlagsAt(grid, gameSettings, x, y);
    const adjacentMines = grid[y][x].adjacentMines;

    if (adjacentFlags === adjacentMines) {
      for (let i = (x === 0 ? 0 : -1); i <= ((x+1) === gameSettings.columns ? 0 : 1); i++)
        for (let j = (y === 0 ? 0 : -1); j <= ((y+1) === gameSettings.rows ? 0 : 1); j++)
            clearTileAt(x + i, y + j, newGrid);
    }

    // update the grid
    setGrid(newGrid);
  }

  const leftClickTile = (x, y) => {
    // copy the grid
    let newGrid = grid.map(row => row.map(tile => ({...tile})));

    // if this is the first click, generate mines
    if (gameState === 'notStarted') {
      generateGridWithMines(gameSettings, x, y, newGrid);
      setGameState('started');
      setClock(setInterval(() => setSeconds(prevSeconds => prevSeconds + 1), 1000));
    }

    // clear the tile (if applicable) and adjacent tiles (if applicable)
    if (gameState === 'notStarted' || gameState === 'started')
      clearTileAt(x, y, newGrid);

    // update the grid
    setGrid(newGrid);
  };

  const rightClickTile = (x, y) => {
    const oldTile = grid[y][x];

    if ((oldTile.status === 'hidden' || oldTile.status === 'flagged')
        && (gameState === 'notStarted' || gameState === 'started')) {
      
      const newTile = {
        ...oldTile,
        status: oldTile.status === 'hidden' ? 'flagged' : 'hidden'
      };

      const newGrid = grid.map((row, i) =>
        row.map((tile, j) => i === y && j === x ? newTile : tile)
      );
  
      setGrid(newGrid);
      setFlagsUsed(flagsUsed + ((oldTile.status === 'hidden') ? 1 : -1));
    }
  };

  const startNewGame = gameSettings => {
    setGrid(generateEmptyGrid(gameSettings));
    setGameState('notStarted');
    setFlagsUsed(0);
    setGameSettings(gameSettings);

    if (clock !== 0)
      clearInterval(clock);
    setSeconds(0);
  };

  const displayTime = () => seconds > 999 ? '999' : zeroPad(seconds, 3);

  const displayFlags = () => zeroPad(flagsUsed, 2);

  return (
    <div className="App">
      <Snackbar open={!!statusMessage} autoHideDuration={5000} onClose={() => setStatusMessage('')}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}>
      <MuiAlert elevation={6}
                variant="filled"
                onClose={() => setStatusMessage('')}
                severity={statusSeverity}>
        {statusMessage}
      </MuiAlert>
    </Snackbar>
      <header className="App-header">
        <h1>Minesweeper 5</h1>
        <div id="difficultyButtonsRow">
          {
            difficultyLevels.map(level =>
              <DifficultyButton
                text={level.name}
                gameSettings={level}
                startNewGame={startNewGame}
                selected={gameSettings === level}
              />
            )
          }
        </div>
        <table className="faceRow">
          <tbody>
            <tr>
              <td style={{ textAlign: 'left'}}>Flags: {displayFlags()} / {gameSettings.mines}</td>
              <td><Face gameState={gameState} startNewGame={() => startNewGame(gameSettings)} /></td>
              <td style={{ textAlign: 'right'}}>Time: {displayTime()}</td>
            </tr>
          </tbody>
        </table>
        <table className="grid">
          <tbody>
            {
              grid.map((row, i) => <tr key={`row_${i}`}>
                {
                  row.map((tile, j) => <td key={`tile_${j}`}>
                    <Tile
                      tile={tile}
                      i={i}
                      j={j}
                      leftClickTile={leftClickTile}
                      rightClickTile={rightClickTile}
                      comboClickTile={comboClickTile}
                      longPressTile={longPressTile}
                      debug={debug}
                    />
                  </td>)
                }
              </tr>)
            }
          </tbody>
        </table>
        <small>
          &copy; <a href="https://dandago.com/">Daniel D'Agostino</a> 2021-{new Date().getFullYear()}
          <br />Part of <a href="https://www.swordsandsoftware.com/mslegacy/">The Minesweeper Legacy</a>
          <br />📝 <a href="https://bitbucket.org/dandago/minesweeper-5-react/src/master/">Source code / readme</a>
        </small>
      </header>
    </div>
  );
}

export default App;
