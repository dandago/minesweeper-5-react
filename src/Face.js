import React from 'react';
import { Button, Tooltip } from '@material-ui/core';

const Face = props => {

  const getFace = gameState => {
    switch (gameState) {
      case 'won':
        return '😎';
      case 'lost':
        return '😞';
      default:
        return '😀';
    }
  }

  return <Tooltip title="Start new game">
          <Button
            color="default"
            variant="contained"
            onClick={props.startNewGame}
            style={{fontSize: '16pt', padding: '0'}}
          >
            {getFace(props.gameState)}
          </Button>
         </Tooltip>;
};

export default Face;