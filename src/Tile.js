import React, { useState } from 'react';
import { Button } from '@material-ui/core';

const Tile = props => {

  const [leftMouseButtonDown, setLeftMouseButtonDown] = useState(false);
  const [rightMouseButtonDown, setRightMouseButtonDown] = useState(false);
  const [leftMouseButtonDownDate, setLeftMouseButtonDownDate] = useState(0);

  const tileButtonSize = '28px';
  const tileButtonStyle = {
    maxWidth: tileButtonSize,
    maxHeight: tileButtonSize,
    minWidth: tileButtonSize,
    minHeight: tileButtonSize,
    color: 'black',
    backgroundColor: 'white',
    fontWeight: 'bold',
    fontSize: '14pt',
    padding: '1px',
    display: 'block'
  };

  const getTileView = (tile) => {
    if (tile.status === 'flagged') {
      if (tile.wrong === true)
        return '❌';
      else
        return '🏴‍☠️';
    }
    else if (tile.status === 'shown' || props.debug === true) {
      if (tile.mined)
        return tile.exploded ? '💥' : '💣';
      else
        return tile.adjacentMines;
    }
    else if (tile.status === 'hidden')
        return ' ';
    else
      return '!'; // not supposed to happen
  };

  const getTileStyle = tile => {
    if (tile.exploded)
      return {...tileButtonStyle, backgroundColor: 'red'};
    else if (tile.status === 'flagged')
      return {...tileButtonStyle};
    else if (tile.status === 'shown') {
      switch (tile.adjacentMines) {
        case 0: return {...tileButtonStyle, color: 'transparent', backgroundColor: '#AAA'};
        case 1: return {...tileButtonStyle, color: 'rgb(0, 0, 255)', backgroundColor: '#AAA'};
        case 2: return {...tileButtonStyle, color: 'rgb(0, 122, 0)', backgroundColor: '#AAA'};
        case 3: return {...tileButtonStyle, color: 'rgb(255, 0, 0)', backgroundColor: '#AAA'};
        case 4: return {...tileButtonStyle, color: 'rgb(0, 0, 122)', backgroundColor: '#AAA'};
        case 5: return {...tileButtonStyle, color: 'rgb(122, 0, 0)', backgroundColor: '#AAA'};
        case 6: return {...tileButtonStyle, color: 'rgb(0, 122, 122)', backgroundColor: '#AAA'};
        case 7: return {...tileButtonStyle, color: 'rgb(122, 0, 122)', backgroundColor: '#AAA'};
        case 8: return {...tileButtonStyle, color: 'rgb(0, 0, 0)', backgroundColor: '#AAA'};
        default: return {...tileButtonStyle, backgroundColor: 'pink'}; // should not happen
      }
    }
    else
      return tileButtonStyle;
  };

  const handleMouseDown = e => {
    const leftMouseButtonDown = e.buttons & 1;
    const rightMouseButtonDown = e.buttons & 2;

    setLeftMouseButtonDown(leftMouseButtonDown);
    setRightMouseButtonDown(rightMouseButtonDown);

    if (leftMouseButtonDown > 0)
      setLeftMouseButtonDownDate(new Date());
  };

  const handleMouseUp = e => {
    const nowLeftMouseButtonDown = e.buttons & 1;
    const nowRightMouseButtonDown = e.buttons & 2;

    // left click
    if (leftMouseButtonDown && !nowLeftMouseButtonDown)
    {
      // hold left + right click
      if (rightMouseButtonDown && nowRightMouseButtonDown)
        props.comboClickTile(props.j, props.i);
      // just a right click
      else {
        doRightOrComboClick();
      }
    }
    // right click
    else if (rightMouseButtonDown && !nowRightMouseButtonDown)
    {
      // hold left + right click
      if (leftMouseButtonDown && nowLeftMouseButtonDown)
        props.comboClickTile(props.j, props.i);
      // just a right click
      else
        props.rightClickTile(props.j, props.i);
    }

    setLeftMouseButtonDown(nowLeftMouseButtonDown);
    setRightMouseButtonDown(nowRightMouseButtonDown);
    return false;
  };

  const handleTouchStart = () => {
    setLeftMouseButtonDownDate(new Date());
  }

  const doRightOrComboClick = () => {
    const now = new Date();
    const elapsed = now - leftMouseButtonDownDate;

    setLeftMouseButtonDownDate(0);
    if (elapsed > 500) // longpress
      props.longPressTile(props.j, props.i);
    else
      props.leftClickTile(props.j, props.i);
  }

  return <Button
            variant="contained"
            onContextMenu={e => { e.preventDefault(); }}
            onTouchStart={handleTouchStart}
            onTouchEnd={doRightOrComboClick}
            onTouchCancel={doRightOrComboClick}
            onMouseUp={handleMouseUp}
            onMouseDown={handleMouseDown}
            style={getTileStyle(props.tile)}
          >
            { getTileView(props.tile) }
          </Button>
};

export default Tile;