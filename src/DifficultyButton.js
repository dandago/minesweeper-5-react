import React from 'react';
import { Button } from '@material-ui/core';

const DifficultyButton = props => {
  const text = props.text;
  const gameSettings = props.gameSettings;
  const startNewGame = props.startNewGame;
  const selected = props.selected;

  return <Button
    variant="contained"
    color={ selected ? "secondary" : "primary" }
    onClick={() => startNewGame(gameSettings)}
    style={{ marginLeft: '2px', marginRight: '2px' }}
  >
    {text}
  </Button>
};

export default DifficultyButton;