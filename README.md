# Introduction

Welcome to Minesweeper 5! [Play online](https://www.swordsandsoftware.com/mslegacy/minesweeper-5/).

This is a Minesweeper clone built with React by [Daniel D'Agostino](https://dandago.com/) over a weekend (20th-21st March 2021). It is also the fifth in a series of [Minesweeper
clones](https://www.swordsandsoftware.com/mslegacy/) since 2004.

# How to play the game

## Starting

* A number of randomly-generated mines are hidden in a grid
* Click on a tile to reveal it
* The first tile you click never contains a mine
* You can choose Beginner, Intermediate or Expert difficulty levels
* Click on the face button anytime you want to restart the game with the same settings

## Playing

* Revealing a tile (that isn't a mine) will show how many mines are next to it in all 8 directions
* If there are no mines nearby, the area is cleared until mines are nearby
* Right-click (or long-press) to flag/unflag a tile where you think a mine is concealed
* Flagged tiles will not blow up if you left-click on them
* If a tile shows a number (e.g. 3) and you have flagged that many tiles around it, click both the left and right mouse buttons simultaneously (or long-press) the number to clear tiles around it

## Losing

* You lose instantly if you click on a mine
* When you lose, all mines are shown
* The mine that exploded is highlighted
* If you incorrectly flagged a tile (i.e. there is no mine), a cross is shown in its place

## Winning

* Clear all tiles that don't contain mines in order to win the game
* They don't necessarily need to be flagged
* Try to beat the game while achieving the fastest time for each difficulty level

# Things I didn't bother to implement

* Question marks
* High score list
* Custom-sized game
* Keyboard shortcuts

# How to run the game (for developers)

In the project directory, you can run:

## `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
